// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.bin2txt;

import static org.refcodes.cli.CliSugar.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.codec.BaseBuilder;
import org.refcodes.codec.BaseDecoderInputStream;
import org.refcodes.codec.BaseEncoderOutputStream;
import org.refcodes.codec.BaseMetrics;
import org.refcodes.codec.BaseMetricsConfig;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.exception.BugException;
import org.refcodes.io.LineBreakOutputStream;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.numerical.NumericalUtility;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.runtime.Execution;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * A minimum REFCODES.ORG enabled command line interface (CLI) application. Get
 * inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "bin2txt";
	private static final String TITLE = ":::" + NAME.toUpperCase() + ":::";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/bin2txt_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String DESCRIPTION = "Encoding and decoding tool for BASE64 and related encodings/decodings from/to ASCII to/from binary (see [https://www.metacodes.pro/manpages/bin2txt_manpage]).";
	private static final String BYTES_PROPERTY = "bytes";
	private static final String DECODE_PROPERTY = "decode";
	private static final String ENCODE_PROPERTY = "encode";
	private static final String TEXT_PROPERTY = "text";
	private static final String OUTPUTFILE_PROPERTY = "outputFile";
	private static final String INPUTFILE_PROPERTY = "inputFile";
	private static final String ENCODING_PROPERTY = "encoding";
	private static final String HEX_PROPERTY = "hex";
	private static final String LINE_WIDTH_PROPERTY = "lineWidth";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final EnumOption<BaseMetricsConfig> theEncodingArg = enumOption( "encoding", BaseMetricsConfig.class, ENCODING_PROPERTY, "The BASE (e.g. BASE64) encoding/decoding to be applied for handling encoded data: " + VerboseTextBuilder.asString( BaseMetricsConfig.values() ) );
		final Flag theHexFlag = flag( "hex", HEX_PROPERTY, "Use a hexadecimal representation of (binary) output." );
		final StringOption theInputFileArg = stringOption( 'i', "input-file", INPUTFILE_PROPERTY, "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", OUTPUTFILE_PROPERTY, "The output file which to process to." );
		final StringOption theTextArg = stringOption( 't', "text", TEXT_PROPERTY, "The text message which to process." );
		final StringOption theBytesArg = stringOption( 'b', "bytes", BYTES_PROPERTY, "The message in bytes (e.g. \"127, 128, 0x10, 0xFF\") which to process." );
		final IntOption theLineWidthArg = intOption( "line-width", LINE_WIDTH_PROPERTY, "The line width when outputting text." );
		final Flag theEncodeFlag = flag( 'e', "encode", ENCODE_PROPERTY, "Encodes the given message." );
		final Flag theDecodeFlag = flag( 'd', "decode", DECODE_PROPERTY, "Decodes the given message." );
		final Flag theDebugFlag = debugFlag( false );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();

		// @formatter:off
		final Term theArgsSyntax = cases(
			and( 
				xor( theEncodeFlag, theDecodeFlag ), any ( and( xor( theTextArg, theBytesArg ), any( theHexFlag, theEncodingArg, theVerboseFlag, theDebugFlag ) ) ) 
			),
			and(
				xor( and( theEncodeFlag, any ( theLineWidthArg) ), theDecodeFlag ), theInputFileArg, theOutputFileArg, any(  theEncodingArg, theVerboseFlag, theDebugFlag )
			),
			and(
				xor( and( theEncodeFlag, any ( theLineWidthArg) ), theDecodeFlag ), xor( theInputFileArg, and( theOutputFileArg, any ( theVerboseFlag, theDebugFlag ) ) ), any( theEncodingArg )
			),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "Encode a text message", theEncodeFlag, theTextArg, theVerboseFlag ), 
			example( "Decode a text message", theDecodeFlag, theTextArg, theVerboseFlag ),
			example( "Encode a message in bytes", theEncodeFlag, theBytesArg, theVerboseFlag ), 
			example( "Decode a message in bytes", theDecodeFlag, theBytesArg, theVerboseFlag ),
			example( "Encode input file to output file", theEncodeFlag, theInputFileArg, theOutputFileArg, theVerboseFlag ), 
			example( "Decode input file to output file", theDecodeFlag, theInputFileArg, theOutputFileArg, theVerboseFlag ),
			example( "Encode STDIN file to output file", theEncodeFlag, theOutputFileArg ), 
			example( "Decode input file to STDOUT", theDecodeFlag, theInputFileArg ),
			example( "Encode STDIN to STDOUT (BASE64 encoding)", theEncodeFlag),
			example( "Decode STDIN (BASE64 encoding) to STDOUT", theDecodeFlag ),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theCliHelper.isVerbose();

		try {
			final String theText = theArgsProperties.get( theTextArg );
			final byte[] theBytes = theArgsProperties.get( theBytesArg ) != null ? NumericalUtility.toBytes( theArgsProperties.get( theBytesArg ) ) : null;
			final String theInputFilePath = theArgsProperties.get( theInputFileArg );
			final String theOutputFilePath = theArgsProperties.get( theOutputFileArg );
			final boolean isHex = theArgsProperties.getBoolean( theHexFlag );
			final int theLineWidth = theArgsProperties.getIntOr( theLineWidthArg, -1 );
			final boolean isEncode = theArgsProperties.getBoolean( theEncodeFlag );
			final boolean isDecode = theArgsProperties.getBoolean( theDecodeFlag );
			final BaseMetricsConfig theBaseMetrics = theArgsProperties.getEnumOr( BaseMetricsConfig.class, theEncodingArg, BaseMetricsConfig.BASE64 );

			if ( isVerbose ) {
				if ( theText != null && theText.length() != 0 ) {
					LOGGER.info( "Text = " + theText );
				}
				if ( theBytes != null && theBytes.length != 0 ) {
					LOGGER.info( "Bytes = { " + NumericalUtility.toHexString( ", ", theBytes ) + " }" );
				}
				if ( theBaseMetrics != null ) {
					LOGGER.info( "Encoding = " + theBaseMetrics );
				}
				if ( theLineWidth != -1 ) {
					LOGGER.info( "Line width = " + theLineWidth );
				}
				LOGGER.info( "Operation = " + ( isEncode ? "Encoding" : ( isDecode ? "Decoding" : "?" ) ) );
			}

			if ( ( theText != null && theText.length() != 0 ) || ( theBytes != null && theBytes.length != 0 ) ) {

				// -------------------------------------------------------------
				// ENCRYPT MESSAGE:
				// -------------------------------------------------------------

				if ( isEncode ) {
					encode( theText, theBytes, theBaseMetrics, isHex, isVerbose );
				}
				// -------------------------------------------------------------
				// DECRYPT MESSAGE :
				// -------------------------------------------------------------
				else if ( isDecode ) {
					decode( theText, theBytes, theBaseMetrics, isHex, isVerbose );
				}
				// -------------------------------------------------------------
				else {
					throw new BugException( "We encountered a bug, none argument was processed." );
				}
			}
			else {
				InputStream theInputStream = Execution.toBootstrapStandardIn();
				OutputStream theOutputStream = Execution.toBootstrapStandardOut();

				if ( theInputFilePath != null && theInputFilePath.length() != 0 ) {
					File theInputFile = new File( theInputFilePath );
					if ( isVerbose ) {
						LOGGER.info( "Input file = \"" + theInputFilePath + "\" (<" + theInputFile.getAbsolutePath() + ">)" );
					}
					if ( !theInputFile.exists() || !theInputFile.isFile() ) {
						throw new FileNotFoundException( "No file \"" + theInputFilePath + "\" (<" + theInputFile.getAbsolutePath() + ">) found!" );
					}
					theInputStream = new FileInputStream( theInputFile );
				}
				if ( theOutputFilePath != null && theOutputFilePath.length() != 0 ) {
					File theOutputFile = new File( theOutputFilePath );
					if ( isVerbose ) {
						LOGGER.info( "Output file = \"" + theOutputFilePath + "\" (<" + theOutputFile.getAbsolutePath() + ">)" );
					}
					//	if ( theOutputFile.exists() || theOutputFile.isFile() ) {
					//		throw new FileAlreadyExistsException( "File \"" + theOutputFileName + "\" (<" + theOutputFile.getAbsolutePath() + ">) already exists!" );
					//	}
					theOutputStream = new FileOutputStream( theOutputFile );
				}

				// -------------------------------------------------------------
				// ENCRYPT INPUT STREAM:
				// -------------------------------------------------------------

				if ( isEncode ) {
					if ( theLineWidth != -1 ) {
						theOutputStream = new LineBreakOutputStream( theOutputStream, theLineWidth );
					}
					theOutputStream = new BaseEncoderOutputStream( theOutputStream, theBaseMetrics );
					encode( theInputStream, theOutputStream, isVerbose );
				}
				// -------------------------------------------------------------
				// DECRYPT INPUT STREAM:
				// -------------------------------------------------------------
				else if ( isDecode ) {
					theInputStream = new BaseDecoderInputStream( theInputStream, theBaseMetrics );
					decode( theInputStream, theOutputStream, isVerbose );
				}
				// -------------------------------------------------------------
				else {
					throw new BugException( "We encountered a bug, none argument was processed." );
				}
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static void encode( InputStream aInputStream, OutputStream aOutputStream, boolean aIsVerbose ) throws IOException {
		try ( InputStream theInputStream = new BufferedInputStream( aInputStream ) ) {
			try ( OutputStream theOutputStream = new BufferedOutputStream( aOutputStream ) ) {
				theInputStream.transferTo( theOutputStream );
			}
		}
	}

	private static void decode( InputStream aInputStream, OutputStream aOutputStream, boolean aIsVerbose ) throws IOException {
		try ( InputStream theInputStream = new BufferedInputStream( aInputStream ) ) {
			try ( OutputStream theOutputStream = new BufferedOutputStream( aOutputStream ) ) {
				theInputStream.transferTo( theOutputStream );
			}
		}
	}

	private static void encode( String aText, byte[] aBytes, BaseMetrics aBaseMetrics, boolean isHex, boolean isVerbose ) {
		final BaseBuilder theBaseBuilder = new BaseBuilder().withBaseMetrics( aBaseMetrics );
		final byte[] theDecodedData = ( aBytes != null ) ? aBytes : aText.getBytes();
		final String theResult = isHex ? "{ " + NumericalUtility.toHexString( ", ", theBaseBuilder.toEncodedText( theDecodedData ).getBytes() ) + " }" : theBaseBuilder.toEncodedText( theDecodedData );
		if ( isVerbose ) {
			LOGGER.info( "Encoded := " + theResult );
		}
		else {
			System.out.println( theResult );
		}
	}

	private static void decode( String aText, byte[] aBytes, BaseMetrics aBaseMetrics, boolean isHex, boolean isVerbose ) {
		final BaseBuilder theBaseBuilder = new BaseBuilder().withBaseMetrics( aBaseMetrics );
		final byte[] theDecoded = theBaseBuilder.toDecodedData( aBytes != null ? new String( aBytes ) : aText );
		final String theResult = isHex ? "{ " + NumericalUtility.toHexString( ", ", theDecoded ) + " }" : new String( theDecoded );
		if ( isVerbose ) {
			LOGGER.info( "Decoded := " + theResult );
		}
		else {
			System.out.println( theResult );
		}
	}
}
