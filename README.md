# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***A command line tool for playing around with BASE text encoding as well as decoding using the [BASE64](https://www.metacodes.pro/blog/base_how_low_can_you_go_base64_base32_base16/) algorithm (alongside BASE2, BASE4, BASE16 or BASE32 encodings).***

## Usage ##

See the [`BIN2TXT`](https://www.metacodes.pro/manpages/bin2txt_manpage) manpage for a complete user guide, basic usage instructions can be queried as follows:

```
$ ./bin2txt-launcher-x.y.z.sh --help
```

## Downloads ##

For a variety of readily built executables please refer to the [downloads](https://www.metacodes.pro/downloads) section of the [`METACODES.PRO`](https://www.metacodes.pro) site.

## Getting started ##

To get up and running, clone the [`funcodes-bin2txt`](https://bitbucket.org/funcodez/funcodes-bin2txt/) repository from [`bitbucket`](https://bitbucket.org/funcodez/funcodes-bin2txt)'s `git` repository.

## How do I get set up? ##

Using `SSH`, go as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`CSV`](https://bitbucket.org/funcodez/funcodes-bin2txt/) project:

```
git clone git@bitbucket.org:funcodez/funcodes-bin2txt.git
```

Using `CSV`, go accordingly as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`CSV`](https://bitbucket.org/funcodez/funcodes-bin2txt/) project:

```
git clone https://bitbucket.org/funcodez/funcodes-bin2txt.git
```

Then you can build a [`fat-jar`](https://maven.apache.org/plugins/maven-shade-plugin/examples/executable-jar.html) and launch the application: 

## Big fat executable bash script (optional) ##

This step is optional, though when running your application under `Linux`, the following will be your friend:

> To build a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script, take a look at the [`scriptify.sh`](https://bitbucket.org/funcodez/funcodes-bin2txt/src/master/scriptify.sh) script and the [`build.sh`](https://bitbucket.org/funcodez/funcodes-bin2txt/src/master/build.sh) script respectively:

```
./scriptify.sh
./target/bin2txt-launcher-x.y.z.sh
```

The resulting `bin2txt-launcher-x.y.z.sh` file is a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script being launched via `./target/bin2txt-launcher-x.y.z.sh`.

> Building and creating an executable bash script is done by calling `./build.sh`!

## First steps ##

Go for `./target/bin2txt-launcher-x.y.z.sh --help` (or `java -jar target/funcodes-bin2txt-0.0.1.jar` if you wish) to get instructions on how to invoke the tool.

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/funcodez/funcodes-bin2txt/issues)
* Add a nifty user-interface
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

This code is written and provided by Siegfried Steiner, Munich, Germany. Feel free to use it as skeleton for your own applications. Make sure you have considered the license conditions of the included artifacts (see the provided `pom.xml` file).

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) artifacts used by this template are copyright (c) by Siegfried Steiner, Munich, Germany and licensed under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.